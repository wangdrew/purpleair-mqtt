module gitlab.com/wangdrew/purpleair-mqtt

go 1.15

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
)
