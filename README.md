# purpleair-mqtt

Periodically reads from a PurpleAir sensor and publishes the value to MQTT

## Config via envvars
```
POLL_INTERVAL=5
MQTT_URL=tcp://mqtt:1883
MQTT_TOPIC=purpleair
MQTT_USER=
MQTT_PASS=
PURPLE_AIR_SENSOR_ID=63635
```

## Running in docker compose:
```
version: "3"
services:
  purpleair-mqtt:
    image: registry.gitlab.com/wangdrew/purpleair-mqtt
    container_name: purpleair-mqtt
    env_file: conf.env
    restart: unless-stopped
    entrypoint: ["/purpleair-mqtt"]
```
