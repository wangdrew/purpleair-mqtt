package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	MqttURL           string `envconfig:"MQTT_URL" default:"tcp://mqtt:1883"`
	MqttTopic         string `envconfig:"MQTT_TOPIC" default:"purpleair"`
	MqttUser          string `envconfig:"MQTT_USER" default:""`
	MqttPass          string `envconfig:"MQTT_PASS" default:""`
	PurpleAirSensorID string `envconfig:"PURPLE_AIR_SENSOR_ID" default:"63635"`
	PollInterval      int    `envconfig:"POLL_INTERVAL" default:"600"`
}

type PurpleAirResponse struct {
	Results []PurpleAirMetric `json:"results"`
}

type PurpleAirMetric struct {
	ID        int    `json:"ID"`
	Label     string `json:"Label"`
	LastSeen  int64  `json:"LastSeen"`
	PM1_0Str  string `json:"pm1_0_atm"`
	PM2_5Str  string `json:"pm2_5_atm"`
	PM10_0Str string `json:"pm10_0_atm"`
	PM10      float64
	PM25      float64
	PM100     float64
}

type ComputedMetric struct {
	LastSeenTs time.Time `json:"LastSeenTs"`
	PM25       float64   `json:"PM25"`
	PM100      float64   `json:"PM100"`
	AQI        int       `json:"AQI"`
	AQILabel   string    `json:"AQILabel"`
}

func main() {
	log.Printf("starting PurpleAir-MQTT consumer")
	var (
		conf       = Config{}
		ctx        = context.Background()
		toMqtt     = make(chan ComputedMetric, 100)
		stopSignal = make(chan struct{})
	)
	if err := envconfig.Process("", &conf); err != nil {
		log.Fatal(err)
	}
	log.Printf("configuration: %+v\n", conf)

	//mqtt producer
	h, _ := os.Hostname()
	mqttClientID := fmt.Sprintf("purpleAirPoller-%s", h)
	topic := conf.MqttTopic + "/" + conf.PurpleAirSensorID
	mqttOutput := NewMqtt(conf.MqttURL, mqttClientID, topic, conf.MqttUser, conf.MqttPass)
	go func() {
		mqttOutput.Start(toMqtt, stopSignal)
	}()

	timer := time.NewTicker(time.Duration(conf.PollInterval) * time.Second)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-timer.C:
				metrics, err := pollPurpleAir(conf)
				if err != nil {
					log.Printf("Failed to poll PurpleAir: %+v", err)
					continue
				}
				computed := computeAggregateAqi(metrics)
				toMqtt <- computed
			}
		}
	}()

	<-ctx.Done()
	timer.Stop()
	close(stopSignal)
}

func pollPurpleAir(c Config) ([]PurpleAirMetric, error) {
	url := fmt.Sprintf("https://www.purpleair.com/json?show=%s", c.PurpleAirSensorID)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var respJSON PurpleAirResponse
	err = json.Unmarshal(body, &respJSON)
	if err != nil {
		return nil, err
	}
	if len(respJSON.Results) == 0 {
		return nil, errors.New("Empty results in purpleAir JSON response. Perhaps the API changed?")
	}
	for i, m := range respJSON.Results {
		pm25Float, err := strconv.ParseFloat(m.PM2_5Str, 64)
		if err != nil {
			return nil, errors.New("Non float value in JSON field 'PM2_5Value'")
		}
		respJSON.Results[i].PM25 = pm25Float
		pm100Float, err := strconv.ParseFloat(m.PM10_0Str, 64)
		if err != nil {
			return nil, errors.New("Non float value in JSON field 'PM2_5Value'")
		}
		respJSON.Results[i].PM100 = pm100Float
		pm10Float, err := strconv.ParseFloat(m.PM1_0Str, 64)
		if err != nil {
			return nil, errors.New("Non float value in JSON field 'PM2_5Value'")
		}
		respJSON.Results[i].PM10 = pm10Float
	}

	return respJSON.Results, nil
}

var PMToAqiLookup = map[string][][]float64{
	"pm25":  PM25ToAqiLookup,
	"pm100": PM100ToAqiLookup,
}

// Clow, Chigh, Ilow, Ihigh
var PM25ToAqiLookup = [][]float64{
	{
		0.0, 12.0, 0, 50,
	}, {
		12.1, 35.4, 51, 100,
	}, {
		35.5, 55.4, 101, 150,
	}, {
		55.5, 150.4, 151, 200,
	}, {
		150.5, 250.4, 201, 300,
	}, {
		250.5, 350.4, 201, 400,
	}, {
		350.5, 500.4, 401, 500,
	},
}

// Clow, Chigh, Ilow, Ihigh
var PM100ToAqiLookup = [][]float64{
	{
		0.0, 54, 0, 50,
	}, {
		55, 154, 51, 100,
	}, {
		155, 254, 101, 150,
	}, {
		255, 354, 151, 200,
	}, {
		355, 424, 201, 300,
	}, {
		425, 504, 201, 400,
	}, {
		505, 604, 401, 500,
	},
}

func AqiLabel(aqi int) string {
	switch {
	case aqi <= 50:
		return "Good"
	case aqi <= 100:
		return "Moderate"
	case aqi <= 150:
		return "Unhealthy for Sensitive Groups"
	case aqi <= 200:
		return "Unhealthy"
	case aqi <= 300:
		return "Very Unhealthy"
	default:
		return "Hazardous"
	}
}

func computeAggregateAqi(metrics []PurpleAirMetric) ComputedMetric {
	var avgPM25, avgPM100 float64
	for _, m := range metrics {
		avgPM25 += m.PM25
		avgPM100 += m.PM100
	}
	avgPM25 /= float64(len(metrics))
	avgPM100 /= float64(len(metrics))
	aqi := int(math.Max(computeAqi("pm25", avgPM25), computeAqi("pm100", avgPM100)))

	return ComputedMetric{
		LastSeenTs: time.Unix(metrics[0].LastSeen, 0),
		PM25:       avgPM25,
		PM100:      avgPM100,
		AQI:        aqi,
		AQILabel:   AqiLabel(aqi),
	}
}

func computeAqi(pollutant string, avgPM float64) float64 {
	lookup := PMToAqiLookup[pollutant]
	aqiBracket := 0
	for _, vals := range lookup {
		if avgPM > vals[1] {
			aqiBracket += 1
		} else {
			break
		}
	}
	if aqiBracket > 6 {
		return 500 // beyond AQI scale
	}
	aqiLvl := lookup[aqiBracket]
	cLo, cHi, iLo, iHi := aqiLvl[0], aqiLvl[1], aqiLvl[2], aqiLvl[3]
	return (((iHi - iLo) / (cHi - cLo)) * (avgPM - cLo)) + iLo
}
