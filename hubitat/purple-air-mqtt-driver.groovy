import groovy.json.JsonSlurper

/**
*/
metadata {
    definition(name: "AQI Sensor", namespace: "community", author: "Andrew Wang", importUrl: "..", description: "Displays AQI air quality values") {
        capability "Initialize"
        attribute "pm25Index", "number"
        attribute "aqi", "number"
        attribute "aqilabel", "string"
    }
}
    
preferences {
    input name: "logEnable", type: "bool", title: "Enable debug logging", required: true, defaultValue: true
    input name: "deviceHostname", type: "text", title: "URL of the MQTT message bus", required: true, defaultValue: "192.168.69.5"
    input name: "topicSub", type: "text", title: "mqtt topic for airquality data", required: true, defaultValue: "purpleair"
}
def logsOff(){
    log.warn "debug logging disabled..."
    device.updateSetting("logEnable",[value:"false",type:"bool"])
}

def updated(){
    log.info "updated!!"
    initialize()
    if (logEnable) runIn(1800,logsOff)
}

def installed() {
    log.info "installed!!"
    updated()
}

def healthcheck() {
    if (!interfaces.mqtt.isConnected()) {
        log.info "mqtt is down - attempting reconnection"
        initialize()
    }
    runIn(300, healthcheck)
}

def parse(String description){
    def result = null
    def parser = new JsonSlurper()
    def jsondata = interfaces.mqtt.parseMessage(description).payload
    try {
        def data = parser.parseText(jsondata)
        aqi = Math.round(Float.parseFloat("${data.AQI}"))
        pm25 = String.format("%.1f", Float.parseFloat("${data.PM25}"))
        String aqiLabel = ""
        if (aqi < 50) {
            aqiLabel = "Good - ${aqi}"
        } else if (aqi < 100) {
            aqiLabel = "Moderate - ${aqi}"
        } else if (aqi < 150) {
            aqiLabel = "Sensitive - ${aqi}"
        } else if (aqi < 200) {
            aqiLabel = "Unhealthy - ${aqi}"
        } else if (aqi < 300) {
            aqiLabel = "V. Unhealthy - ${aqi}"
        } else {
            aqiLabel = "Hazardous - ${aqi}"
        }
        sendEvent(name: "aqi", value: "${aqiLabel}")
        sendEvent(name: "pm25", value: "${pm25}")

    } catch(e) {
        if (logEnable) log.debug "Parse error: ${e.message}"
    }
}

def initialize() 
{   log.info "intialized!"
    try {
        String mqttbroker = "tcp://" + settings?.deviceHostname + ":1883"
        String clientname = "hubitat_airquality" + new Date().getTime()
        interfaces.mqtt.disconnect() // cleanup old connection if any
        interfaces.mqtt.connect(mqttbroker, clientname , null, null)
        pauseExecution(1000) //give it a chance to start
        log.info "Connection established"

        if (logEnable) log.debug "Subscribed to: ${settings?.topicSub}"
        interfaces.mqtt.subscribe(settings?.topicSub)
    } catch(e) {
        if (logEnable) log.debug "Initialize error: ${e.message}"
    }
    runIn(300, healthcheck)
}

def uninstalled() {
    if (logEnable) log.info "Disconnecting from mqtt"
    interfaces.mqtt.disconnect()
}
